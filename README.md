# README #

This README would normally document whatever steps are necessary to get your application up and running.

Breve descrição do projecto

Elaborei o projecto mas e conclui quase todos os pontos descritos no enunciado, tendo em conta que trabalho durante o dia
acabei por desenvolvi esta webapp em duas noites, embora seria necessário mais tempo para o concluir de uma forma completa
e fechada, Parece-me no entanto que para um processo de candidatura é suficiente, permite ver e analisar código e ter em
consideração soluções encontradas.

Pressupostos sobre o projecto:

 Não utilizei uma base de dados (por ex: MySql), utilizei dados "hard-coded"
 Utilizei algumas funções que já havia desenvolvido para outro site (obviamente retirei todas as referÊncias)
 Utilizei livrarias opensource desponiveis na internet (por ex: keypad virtual)
 Entendo que, se existem recursos, devemos usa-los em vez de estar constanmente a "inventar a roda"
 Considero o reaproveitamento de código ser uma boa prática (desde que saibamos o que estamos a fazer!)
 Inclui algum código de CSS, html, javascript
 
 
 Instalação:
 todo o projecto corre localmente dentro de uma pasta e num browser, portanto não requere instalação
 
 
 Notas:
 
 Seria necessário mais tempo para concluir todo o projecto incluindo a documentação, mas pretendia faze-lo até ao dia de hoje
 (antes do Natal, dead line que tinha estabelecido para mim próprio), embora acredito que o que está feito dá para fazer uma 
 avaliação e ponderar em relação a outros candidatos.
 Algumas funcionalidades não estão totalmente funcionais, deixei-as assim mesmo.
 



### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact